# AnimeFaceNotebooks
Notebooks and some data for playing with animeface stylegan2 and deepdanbooru. Versions for colab included (colab/ directory, or links below)

## Usage
### 1. Docker build

* for experiment:
```
docker build --no-cache -f Dockerfile -t docker.maum.ai:443/brain/animeface:v1.0.1 .  
```

* service: 
```
docker build --no-cache -f Dockerfile_service -t docker.maum.ai:443/brain/animeface:v1.0.1-server .  
```

### 2. Running server:
```
docker run -itd --ipc=host -p 39989:39989 --gpus='"device=0"' --name anime_server docker.maum.ai:443/brain/animeface:v1.0.1-server
```
서버 시작에 시간이 조금 걸림에 주의. 
 
### 3. Inference Methods
현재 `new_sample`, `project`, `move_style` 의 3가지 method를 지원합니다(`--method`)  
1. `[new_sample]`: 새로운 anime 이미지를 생성합니다  
args:
    - `--path_image_output`: 생성한 이미지를 저장할 경로
    - `--path_latent_output`: 생성한 이미지에 대응하는 latent 파일을 저장할 경로  
    
    example: `python client.py --method new_sample`

2. `[project]`: 주어진 이미지를 비슷한 모습의 anime 이미지로 사영하고, 대응하는 latent 파일을 얻습니다.   
    args:
    - `--path_image_input`: 사영할 이미지의 경로
    - `--path_image_output`: 사영한 이미지를 저장할 경로
    - `--path_latent_output`: 생성한 이미지에 대응하는 latent 파일을 저장할 경로
    - `--step_project`: 사영하는데 사용할 iteration 수.(default=500)
    
    example: `python client.py --method project --path_image_input samples/one.png --path_image_output project.png --path_latent_output project.pkl`
   
    비고:
    - 얼굴이 이미지의 대부분을 차지해야 좋음.
    - 512 x 512 이미지를 넣어야 함.
    - (step_project / 8.5)s 정도 필요.
    - 기울어지지 않고, 정면을 바라보는 이미지.

3. `[move_style]`: latent 파일과 tag들을 이용해 image를 변화시키고 대응하는 latent 파일을 얻습니다.  
    args:
    - `--path_latent_input`: 사용할 latent 파일의 경로
    - `--path_tags_input`: 조절할 tag들과 값들이 string으로 저장된 파일의 경로(example: tags.txt)
    - `--path_image_output`: 생성한 이미지를 저장할 경로
    - `--path_latent_output`: 생성한 이미지에 대응하는 latent 파일을 저장할 경로
    
    example: `python client.py --method move_style --path_latent_input project.pkl --path_image_output move.png --path_latent_output move.pkl --path_tags_input tags.txt`

```
python client.py --help
```

#### FAQ
- tag 목록은 `https://github.com/RF5/danbooru-pretrained/raw/master/config/class_names_6000.json`에서 가져왔으며,  
 기본적으로 사용할 수 있는 tag들은 
 ```
[':d', ':o', 'ahoge', 'animal_ears', 'aqua_eyes', 'artist_name',
 'bare_shoulders', 'black_border', 'black_hair', 'blonde_hair', 'blue_background', 
'blue_eyes', 'blunt_bangs', 'border', 'bow', 'breasts', 'brown_eyes', 'brown_hair',
 'cat_ears', 'choker', 'circle_cut', 'close-up', 'closed_mouth', 'collarbone', 'collared_shirt',
 'dress', 'expressionless', 'eyebrows', 'eyelashes', 'eyes_visible_through_hair',
 'face', 'fang', 'fingernails', 'flower', 'food', 'frame', 'frills',
 'gloves', 'gradient', 'gradient_background', 'green_eyes', 'grey_background',
 'hair_between_eyes', 'hair_bow', 'hair_intakes', 'hair_ornament', 'hair_ribbon',
 'hairband', 'hat', 'heart', 'holding', 'jacket', 'jewelry',
 'letterboxed', 'light_brown_hair', 'lips', 'long_hair', 'long_sleeves', 'necktie', 'nose_blush',
 'open_mouth', 'orange_eyes', 'orange_hair', 'outdoors', 'parted_lips', 'photo',
 'pillarboxed', 'pink_background', 'pink_eyes', 'pink_hair', 'pink_lips', 'ponytail',
 'portrait', 'purple_eyes', 'purple_hair', 'rain', 'red_bow', 'red_eyes', 'red_hair',
 'ribbon', 'school_uniform', 'shikishi', 'shiny', 'shiny_hair', 'shirt', 'short_hair',
 'sidelocks', 'signature', 'silver_hair', 'simple_background', 'sketch', 'sleeveless',
 'smile', 'striped', 'symbol-shaped_pupils', 'tears', 'teeth', 'traditional_media', 'transparent_background',
 'twintails', 'upper_body', 'white_background', 'white_shirt', 'wing_collar', 'yellow_background',
 'yellow_eyes']
``` 
입니다.

특수 tag: `psi-pre`, `psi-post`, `mutate`  
`mutate`:
`psi-post`:

#### Sumbmodules:  

* (Submodule) [StyleGAN2](https://github.com/NVlabs/stylegan2.git)
* (Submodule) [StyleGAN2-pytorch](https://github.com/dhchoi99/stylegan2-pytorch.git)  
  * Forked repo of original StyleGAN2-pytorch, with some commits to use with AnimeFaceNotebooks  

These submodules are needed to run StyleGAN2

-----

* [DeepDanbooru](https://github.com/KichangKim/DeepDanbooru)  
* [RF5/danbooru-pretrained](https://github.com/RF5/danbooru-pretrained) for training own deepdanbooru weight
  
You don't really *****Have to***** clone following repos, but you'll need one of these to train own wieght for latent-tag vectors:

-----
Others:  

* Checkpoint:
  * Pytorch checkpoint(converted, from tf checkpoint of [gwern]())  
    * Link: https://drive.google.com/file/d/1rC5phxSds2lVrSGK4085UBuEUpSdywCN/view?usp=sharing



## Stylegan2 Anime Face model
This notebook has a few simple functions to use stylegan2s projection code to project input images (quality of results varies, but honestly surprisingly good), and sliders for interactively modifying a few tags. The directions in dlatent space were obtained by generating a lot of faces, tagging them with deepdanbooru, and then finding a regression line in disentangled latent space fit to the tag confidence values from deepdanbooru. This version uses Lasso regression to try and exploit sparsity in the disentangled latent space. There's some development notes in the notebook, and code that you can use to calculate your own directions. latents/dlatents + tags for 100000 generated images are included.

<a href="https://colab.research.google.com/github/halcy/AnimeFaceNotebooks/blob/master/colab/Stylegan2_Playground.ipynb" target="_parent"><img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>

Example Video: http://www.youtube.com/watch?v=GRG6czAZql0

[![Stylegan2 Anime Face Interactive Modification](https://img.youtube.com/vi/GRG6czAZql0/0.jpg)](http://www.youtube.com/watch?v=GRG6czAZql0)

## Deepdanbooru + GradCam
This notebook as functions for using DeepDanbooru for tagging, for calculating gradcam maps and post-processing them into activation maps for specific tags, and for then visualizing these maps.

<a href="https://colab.research.google.com/github/halcy/AnimeFaceNotebooks/blob/master/colab/DeepDanbooru-Playground.ipynb" target="_parent"><img src="https://colab.research.google.com/assets/colab-badge.svg" alt="Open In Colab"/></a>

Screenshot:

![Shaw (arknights) with tail highlited using gradcam](https://raw.githubusercontent.com/halcy/AnimeFaceNotebooks/master/example_data/gradcam_tagmaps.png)
