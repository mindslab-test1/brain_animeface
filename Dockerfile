FROM docker.maum.ai:443/brain/vision:v0.2.0-cu101-torch160

ENV project_path=/root/AnimeFace

WORKDIR /root/AnimeFaceNotebooks

COPY setups/requirements.txt setups/requirements.txt

RUN python -m pip --no-cache-dir install --upgrade \
    -r setups/requirements.txt && \
    apt-get clean && \
    apt-get autoremove && \
    rm -rf /tmp/* /workspace/*

COPY setups/requirements_conda.txt setups/requirements_conda.txt

RUN conda install -y --file setups/requirements_conda.txt && \
    conda clean --all

COPY . /root/AnimeFaceNotebooks

RUN bash setups/submodule.sh

RUN bash setups/download.sh
