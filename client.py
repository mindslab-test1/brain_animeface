import argparse
import json
import time

import grpc

from proto.anime_pb2 import ProjectRequest, User2Server
from google.protobuf.empty_pb2 import Empty
from proto.anime_pb2_grpc import (
    AnimeStub
)

CHUNK_SIZE = 1024 * 1024


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--ip', type=str, default='localhost')
    parser.add_argument('--port', type=int, default=39989)

    # 3 methods supported
    # use one method at once
    parser.add_argument('--method', type=str, required=True,
                        help="""
                        supports 'new_sample', 'project', 'move_style'\n
                        
                        [new_sample]: generate new sample anime image\n
                        [project]: project human image to anime image\n
                        [move_style]: move style of anime image with tags\n
                        """)

    # paths for in/output files
    parser.add_argument('--path_image_input', type=str,
                        help='image input file path')
    parser.add_argument('--path_image_output', type=str, default='sample.png',
                        help='path for image output to save')

    parser.add_argument('--path_latent_input', type=str, default='latent.pkl',
                        help='path for latent feature input file')
    parser.add_argument('--path_latent_output', type=str, default='latent.pkl',
                        help='path for latent feature output to save')

    parser.add_argument('--path_tags_input', type=str,
                        help='path to saved tag dict .pkl file')

    # others
    parser.add_argument('--step_project', type=int, default=500,
                        help='steps for iteration in projection')

    args = parser.parse_args()
    return args


class Client:
    def __init__(self, args):
        self.args = args

        self.options = [('grpc.max_message_length', CHUNK_SIZE)]
        self.address = f'{self.args.ip}:{self.args.port}'
        self.channel = grpc.insecure_channel(self.address, options=self.options)
        self.stub = AnimeStub(self.channel)

    def read_message(self, responses):
        img_data = bytearray()
        latent_data = bytearray()

        for response in responses:
            img_buffer = response.img
            img_data.extend(img_buffer)

            latent_buffer = response.latent
            latent_data.extend(latent_buffer)

        return img_data, latent_data

    def inference(self, path_image_output, path_latent_output):
        print(f'sending message to server...')

        responses = self.stub.newSample(Empty())
        img, latent_np = self.read_message(responses)

        with open(path_image_output, 'wb') as f:
            f.write(img)

        with open(path_latent_output, 'wb') as f:
            f.write(latent_np)

        return img, latent_np

    def project(self, path_image_input, path_image_output, path_latent_output, steps):
        with open(path_image_input, 'rb') as f:
            stream_img = f.read()

        def image_iterator(stream):
            for idx in range(0, len(stream), CHUNK_SIZE):
                message = ProjectRequest()
                message.img = stream[idx:idx + CHUNK_SIZE]
                if idx == 0:
                    message.steps = steps
                yield message

        iterator = image_iterator(stream_img)

        responses = self.stub.project(iterator)
        img, latent_np = self.read_message(responses)

        with open(path_image_output, 'wb') as f:
            f.write(img)

        with open(path_latent_output, 'wb') as f:
            f.write(latent_np)

    def moveStyle(self, path_latent_input, path_tags_input, path_image_output, path_latent_output):
        with open(path_tags_input, 'rb') as f:
            tags_str = f.read()
        # print(tags_str)
        # tags = json.loads(tags_str)
        # stream_tags = str(tags).encode('UTF-8')
        stream_tags = tags_str

        with open(path_latent_input, 'rb') as f:
            stream_latent = f.read()

        message = User2Server()
        message.latent = stream_latent
        message.dict = stream_tags

        responses = self.stub.moveStyle(message)
        img, latent_np = self.read_message(responses)

        with open(path_image_output, 'wb') as f:
            f.write(img)

        with open(path_latent_output, 'wb') as f:
            f.write(latent_np)


def main():
    start_time = time.time()
    args = parse_args()
    client = Client(args)

    if args.method == 'new_sample':
        client.inference(args.path_image_output, args.path_latent_output)

    elif args.method == 'project':
        if args.step_project < 100:
            args.step_project = 100
        if args.step_project > 2000:
            args.step_project = 2000
        client.project(args.path_image_input, args.path_image_output, args.path_latent_output, args.step_project)

    elif args.method == 'move_style':
        client.moveStyle(args.path_latent_input, args.path_tags_input, args.path_image_output, args.path_latent_output)

    else:
        print(f'no valid method given(given method {args.method}')
    print('time', time.time() - start_time)


if __name__ == '__main__':
    main()
