import argparse
import ast
import io
import logging
import os
import random
import time

import numpy as np
import PIL.Image
import torch
import torchvision.transforms.functional as TF

from concurrent import futures
import grpc
from proto.anime_pb2 import Server2User
from proto.anime_pb2_grpc import (
    AnimeStub, AnimeServicer, add_AnimeServicer_to_server,
)

from anime_generator import Anime_Generator

CHUNK_SIZE = 1024 * 1024

logging.basicConfig(level=logging.INFO)
logger = logging.getLogger('AnimeFaceNotebooks')

DEFAULT_CKPT_PATH = './stylegan2-pytorch/checkpoint/2020-01-11-skylion-stylegan2-animeportraits-networksnapshot-024664.pt'


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--path_ckpt', type=str,
                        default=DEFAULT_CKPT_PATH)
    parser.add_argument('--path_tags', type=str, default='tagged_dlatents/tag_dirs_cont.pkl')

    parser.add_argument('--device', type=str, default='cuda')
    parser.add_argument('--trunc_num', type=int, default=4096)

    parser.add_argument('--port', type=int, default=39989)

    args = parser.parse_args()
    return args


def img_from_bytes(img_bytes, context):
    try:
        img = PIL.Image.open(io.BytesIO(img_bytes)).convert('RGB')
        return img
    except:
        msg = 'wrong img given'
        logger.error(msg)
        context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
        context.set_details(msg)
        return None


def latent_from_bytes(latent_bytes, context, device='cuda'):
    try:
        latent_data = np.frombuffer(latent_bytes, dtype=np.float32)
        latent_data = latent_data.reshape(-1, 512)
        latent_data = torch.from_numpy(latent_data).float().to(device)
        return latent_data
    except:
        msg = 'wrong latent given'
        logger.error(msg)
        context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
        context.set_details(msg)
        return None


def dict_from_bytes(dict_bytes, context):
    dict_data = dict_bytes.decode('UTF-8')
    dict_data = ast.literal_eval(dict_data)
    return dict_data


def none_response():
    for _ in range(2):
        yield Server2User()


class Server(AnimeServicer):
    def __init__(self, args):
        super(Server, self).__init__()

        try:
            self.ag = Anime_Generator(args)
            logger.info(f'Anime Generator successfully generated with args {args}')
        except:
            logger.error(f'Anime Generator cannot be made with args {args}')
            raise ValueError(f'incorrect args given, input: {args}')

        self.dir_temp = './server_temp'
        os.makedirs(self.dir_temp, exist_ok=True)

        logger.info('server started')

    def make_message(self, img_pil, latent):
        imgByteArr = io.BytesIO()
        img_pil.save(imgByteArr, format='PNG')
        stream_img = imgByteArr.getvalue()

        latent_np = latent.clone().detach().cpu().numpy()
        stream_latent = latent_np.tobytes()

        for idx in range(0, len(stream_img), CHUNK_SIZE):
            message = Server2User()
            message.img = stream_img[idx:idx + CHUNK_SIZE]
            message.latent = stream_latent
            yield message

    def newSample(self, request, context):
        # logger.debug('newSample start')
        img_pil, latent = self.ag.new_sample()
        # logger.debug('newSample finished')
        return self.make_message(img_pil, latent)

    def project(self, request_iterator, context):
        img_data = bytearray()
        steps = 500
        for request in request_iterator:
            img_data.extend(request.img)
            try:
                steps = request.steps
            except:
                pass

        img_data = bytes(img_data)
        logging.debug(f'request: {len(img_data)} byte')

        # check imsize
        MAX_IMSIZE = 4 * CHUNK_SIZE
        if len(img_data) > MAX_IMSIZE:
            msg = f'to big input img size, got: {len(img_data) / 1024.:.2f}KB, should be samller than {MAX_IMSIZE / 1024.:.2f}KB'
            logger.error(msg)
            context.set_code(grpc.StatusCode.INVALID_ARGUMENT)
            context.set_details(msg)

            return none_response()

        img = img_from_bytes(img_data, context=context)
        if img is None:
            return none_response()

        img_tensor = self.ag.projector.transform(img)
        img_pil, latent_in = self.ag.project(img_tensor, steps=steps, truncation=0.5)

        return self.make_message(img_pil, latent_in)

    def moveStyle(self, request, context):
        # logger.debug('moveStyle start')
        latent_data = bytearray()
        dict_data = bytearray()

        latent_data.extend(request.latent)
        dict_data.extend(request.dict)

        latent_data = latent_from_bytes(latent_data, context=context, device=self.ag.device)
        if latent_data is None:
            return none_response()

        dict_data = dict_from_bytes(dict_data, context=context)

        img_pil, latent = self.ag.generate_from_latent(latent_data, dict_data)
        # logger.debug('moveStyle finished')
        return self.make_message(img_pil, latent)


def serve():
    args = parse_args()
    Anime_server = Server(args=args)
    server = grpc.server(futures.ThreadPoolExecutor(max_workers=1, ))
    add_AnimeServicer_to_server(Anime_server, server)
    server.add_insecure_port('[::]:%d' % args.port)
    server.start()

    try:
        while True:
            time.sleep(60 * 60 * 24)
    except KeyboardInterrupt:
        server.stop(0)


if __name__ == '__main__':
    serve()
