import argparse
import numpy as np
import pickle
import PIL.Image
import torch

import sys
sys.path.append('stylegan2-pytorch')

from model import Generator
import projector


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('--path_ckpt', type=str, required=True)
    parser.add_argument('--path_tags', type=str, default='tagged_dlatents/tag_dirs_cont.pkl')

    parser.add_argument('--device', type=str, default='cuda')
    parser.add_argument('--trunc_num', type=int, default=4096)

    args = parser.parse_args()
    return args


class Anime_Generator:
    def __init__(self, args):
        self.device = args.device

        self.projector = projector.Projector(args)

        self.net = Generator(512, 512, 8)
        self.net.load_state_dict(torch.load(args.path_ckpt)['g_ema'])

        self.net = self.net.to(args.device)
        self.net.eval()

        with open(args.path_tags, 'rb') as f:
            self.tag_directions = pickle.load(f)

        with torch.no_grad():
            self.trunc = self.net.mean_latent(args.trunc_num)

    def new_sample(self):
        with torch.no_grad():
            x = torch.randn(1, 512).to(self.device)
            img_out, latent = self.net([x], return_latents=True, truncation=0.7, truncation_latent=self.trunc,
                                       input_is='noise', randomize_noise=False)
            img_pil = PIL.Image.fromarray(projector.make_image(img_out)[0])
        return img_pil, latent

    def generate_from_latent(self, latent, tags):
        # TODO truncate_pre

        for key, value in tags.items():
            try:
                vec = self.tag_directions[key] / np.linalg.norm(self.tag_directions[key])
                latent += torch.from_numpy(vec * value).to(self.device)
            except Exception as e:
                print(e)
                pass

        try:
            truncation = tags['psi_post']
            truncation = float(np.clip(truncation))
        except:
            truncation = 0.7

        try:
            mutate_scale = tags['mutate']
            mutate_scale = float(np.clip(mutate_scale))
        except:
            mutate_scale = 0

        with torch.no_grad():
            mutate_latent = torch.randn_like(latent) * mutate_scale
            latent += mutate_latent

            img_out, latent = self.net([latent], return_latents=True, truncation=truncation, truncation_latent=self.trunc,
                                  input_is='latent+', randomize_noise=False)
            img_pil = PIL.Image.fromarray(projector.make_image(img_out)[0])
        return img_pil, latent

    def project(self, input_image, steps=500, truncation=0.7):
        img_gen, latent_path, latent_in = self.projector.project(self.net, input_image, steps=steps)
        with torch.no_grad():
            img_out, latent = self.net([latent_in], return_latents=True, truncation=truncation, truncation_latent=self.trunc,
                                  input_is='latent', randomize_noise=False)
            img_pil = PIL.Image.fromarray(projector.make_image(img_out)[0])
        return img_pil, latent


if __name__ == '__main__':
    import time

    start_time = time.time()

    args = parse_args()

    ag = Anime_Generator(args)
    # img_pil, latent_ori = ag.new_sample()
    # img_pil.save('temp.png')

    path_src = 'input.png'
    proj_src = ag.projector.transform(PIL.Image.open(path_src).convert('RGB'))
    img_pil, _ = ag.project(proj_src, steps=200)
    img_pil.save('temp_proj.png')

    # tag_dict = {
    #     'silver_hair': 20,
    # }
    # new_pil, new_latent = ag.generate_from_latent(latent_ori, tag_dict)
    # new_pil.save('move.png')

    print('time', time.time() - start_time)
