# get checkpoints
wget --tries=10 --load-cookies /tmp/cookies.txt "https://docs.google.com/uc?export=download&confirm=$(wget --quiet --save-cookies /tmp/cookies.txt --keep-session-cookies --no-check-certificate 'https://docs.google.com/uc?export=download&id=1rC5phxSds2lVrSGK4085UBuEUpSdywCN' -O- | sed -rn 's/.*confirm=([0-9A-Za-z_]+).*/\1\n/p')&id=1rC5phxSds2lVrSGK4085UBuEUpSdywCN" -O './stylegan2-pytorch/checkpoint/2020-01-11-skylion-stylegan2-animeportraits-networksnapshot-024664.pt' && rm -rf /tmp/cookies.txt

# get deepdannbooru json
mkdir DeepDanbooru-pytorch
wget https://github.com/RF5/danbooru-pretrained/raw/master/config/class_names_6000.json
