# submodules
git init

rmdir stylegan2
git rm --cached stylegan2
git submodule add https://github.com/NVlabs/stylegan2.git
cd stylegan2
git checkout -b local dae148b9bb6
cd ..

rmdir stylegan2-pytorch
git rm --cached stylegan2-pytorch
git submodule add https://github.com/dhchoi99/stylegan2-pytorch.git
cd stylegan2-pytorch
git checkout -b local b7a36073ede
cd ..

rmdir DeepDanbooru
git rm --cached DeepDanbooru
git submodule add https://github.com/KichangKim/DeepDanbooru.git
cd DeepDanbooru
git checkout -b local 55e8ed7e5e6
cd ..
